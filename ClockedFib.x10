public class ClockedFib {
    public static def fib(n:Long, result:Clocked[Long]):void {
        if (n < 2) {
            result() = n;
            return;
        }
        val x = new Clocked[Long]();
        val y = new Clocked[Long]();
        async clocked(x.clock) fib(n - 1, x);
        fib(n - 2, y);
        x.next();
        y.next();
        result() = x() + y();
    }
    
    public static def main(s:Rail[String]) {
        val N:Int = s.size > 0 ? Int.parseInt(s(0)) : 15n;
        val x = new Clocked[Long]();
        fib(N, x);
        x.next(); // read the value
        Console.OUT.println("fib(" + N + ") = " + x());
   }
}
