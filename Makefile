X10C ?= x10c
ARMUSC_JAR ?= ../armus-x10/target/armusc-x10.jar
JAVA ?= java
ARMUSC = $(JAVA) -jar $(ARMUSC_JAR)

all: BFS.jar ClockedFib.jar ClockedSieve.jar ClockedArrayFib.jar PrefixClock.jar clock.jar

checked: BFS-C.jar ClockedFib-C.jar ClockedSieve-C.jar ClockedArrayFib-C.jar PrefixClock-C.jar

%-C.jar: %.jar
	$(ARMUSC) $< $@

%.jar: %.x10
	$(X10C) -o $@ $<

clock.jar: x10/lang/Clock.x10
	$(X10C) -o $@ $<

clean:
	rm -f *.jar *.class *.java
