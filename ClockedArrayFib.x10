public class ClockedArrayFib {
    public static def fib(n:Long, r:Rail[Clocked[Long]]):void {
        if (n <= 2) {
            r(n)() = 1;
            return;
        }
        r(n - 2).next(); // if you switch these two instructions,
        r(n - 1).next(); // deadlock
        r(n)() = r(n - 1)() + r(n - 2)();
    }
    
    public static def spawnFib(n:Long, r:Rail[Clocked[Long]]):void {
        if (n < 3) {
            async clocked(r(n).clock) fib(n, r);
        } else {
            async clocked(r(n).clock, r(n - 1).clock, r(n - 2).clock) fib(n, r);
        }
    }
    
    public static def main(s:Rail[String]) {
        finish async {
            val N:Int = s.size > 0 ? Int.parseInt(s(0)) : 500n;
            val buff = new Rail[Clocked[Long]](N + 1, (x:Long) => new Clocked[Long]());
            for (n in 1..N) {
                spawnFib(n, buff);
            }
            async clocked(buff(N).clock) {
                buff(N).next(); // read the value
                Console.OUT.println("fib(" + N + ") = " + buff(N)());
            }
        }
   }
}
